dojo.require("esri.map");

function init() {
	var map = new esri.Map("map",{
		basemap:"topo",
		center:[-122.45,37.75], //long, lat
		zoom:13,
		sliderStyle:"small"
	});
}

dojo.ready(init);